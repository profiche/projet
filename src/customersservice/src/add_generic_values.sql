-- Insertion des données

INSERT INTO customers_schema.customers (name, phone, email, address, city, postalzip, password)
VALUES
    ('Jean Rault', '0385251275', 'jean.rault@aol.fr', '86 quai Saint-Nicolas', 'Auxerre', '89000', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Jeremy Anne', '0719328912', 'jeremyanne6677@outlook.fr', '17 rue Adolphe Wurtz', 'Mulhouse', '68100', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Lou Debard', '0813716915', 'debardlou8171@protonmail.fr', '96, avenue Alain Hamon', 'Brive-la-Gaillarde', '19100', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Marius Quere', '0430862133', 'mariusquere9101@google.fr', '83 Rue Hubert de Lisle', 'Pontarlier', '25300', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Amélie Imbert', '0962516088', 'amimbert@icloud.fr', '70 rue Porte d''Orange', 'Dreux', '28100', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Esteban Delorme', '0623168431', 'estebandelorme8088@outlook.fr', '46 Rue du Limas', 'Bastia', '20200', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Isabella Benson', '0973566263', 'isabellabenson9094@protonmail.fr', '26 rue Goya', 'Saint-Denis', '11310', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Alban Bru', '0435327748', 'albanbru8423@aol.fr', '20 boulevard de la Liberation', 'Pontarlier', '25300', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Amandine Guyot', '0568576444', 'amandine.guyot@yahoo.fr', '75 Avenue Millies Lacroix', 'Sarreguemines', '57200', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Alex Bonnot', '0614817871', 'albonnot@aol.fr', '45 rue des Coudriers', 'Sens', '89100', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Enora Guillaume', '0382815768', 'enorag227@hotmail.fr', '26 rue des six frères Ruellan', 'Talence', '33400', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Alban Jarry', '0758481831', 'albanjarry@protonmail.fr', '29 avenue Voltaire', 'Limoges', '87280', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Elise Pons', '0574160862', 'elisepons@aol.fr', '46 rue Marguerite', 'Lambersart', '59130', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Marine Laval', '0820232262', 'mlaval8996@aol.fr', '27 Boulevard de Normandie', 'Bastia', '20200', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Bilal Rosier', '0226506032', 'bilalrosierr@outlook.fr', '47 rue du Faubourg National', 'Ajaccio', '20000', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Andréa Dupas', '0476151219', 'adupas@hotmail.fr', '19 rue Gustave Eiffel', 'Aurillac', '15000', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Julian Collignon', '0664748546', 'juliancollignon@yahoo.fr', '22 avenue de Bouvines', 'Dreux', '28100', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Alice Arnoux', '0596244227', 'alicearnous@protonmail.fr', '82 place Stanislas', 'Soissons', '02200', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Myriam Bardet', '0513778536', 'myriambardet1804@hotmail.fr', '42 rue du Fossé des Tanneurs', 'Courbevoie', '92400', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6'),
    ('Estelle Picot', '0875736306', 'estellepicot@aol.fr', '93 rue de l''Epeule', 'Mulhouse', '68100', '$2b$12$Va88hhuS/QXsJGb5n5O6jexABKItN17Wxk5qKGflrPbm8P54gMpl6');