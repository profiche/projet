-- Créer le schéma "customers_schema"
CREATE SCHEMA IF NOT EXISTS customers_schema;

-- Créer la table "customers" dans le schéma "customers_schema"
CREATE TABLE customers_schema.customers (
    id serial PRIMARY KEY,
    name varchar(255),
    phone varchar(10),
    email varchar(50),
    address text,
    city varchar(50),
    postalzip varchar(5),
    password varchar(255)
);