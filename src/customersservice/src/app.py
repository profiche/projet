import flask
import flask_restx
import psycopg2
from flask_restx import fields
from prometheus_client import make_wsgi_app, Counter
from werkzeug.middleware.dispatcher import DispatcherMiddleware
import os

app = flask.Flask(__name__)
app.config['ERROR_404_HELP'] = False
app.config["SECRET_KEY"] = "69380d2425c27351894bde6886ec7c7f"
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {"/metrics": make_wsgi_app()})
api = flask_restx.Api(app=app,
                      title="API Gestion d'utilisateurs", version="0.1",
                      description="API simple de gestion des utilisateurs",
                      contact_email="Alan.Salmon@etu.univ-grenoble-alpes.fr")

API_KEY = os.getenv("API_KEY", "customers")
POSTGRES_HOST = os.getenv("POSTGRES_HOST", "localhost")
POSTGRES_PORT = os.getenv("POSTGRES_PORT", "5432")
POSTGRES_DB = os.getenv("POSTGRES_DB", "customers_db")
POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")


db_params = {
    'dbname': POSTGRES_DB,
    'user': POSTGRES_USER,
    'password': POSTGRES_PASSWORD,
    'host': POSTGRES_HOST,
    'port': int(POSTGRES_PORT)
}

customer_model = api.model('Product', {
    "id": fields.String,
    "name": fields.String,
    "phone": fields.String,
    "email": fields.String,
    "address": fields.String,
    "city": fields.String,
    "postalzip": fields.String,
    "password": fields.String
})

REQUESTS_SUCCESSFUL = Counter("requests_successful", "Number of successful requests", labelnames=["customer_id"])
REQUESTS_FAILED = Counter("requests_failed", "Number of failed requests")


# Connexion à la base de données
conn = psycopg2.connect(**db_params)


def check_api_key(headers):
    H_API_KEY = headers.get("X-API-KEY")
    if not H_API_KEY:
        return False
    return H_API_KEY == API_KEY


@app.route('/')
def index():
    """Route par défaut, affichant la documentation Swagger de l'API.

    .. http:get:: /

       :statuscode 200: sans erreur
    """
    return """API de customers""", 200


@api.param('customer_id', 'L\'ID d\'un utilisateur')
@api.route('/customer/<string:customer_id>')
class CustomerID(flask_restx.Resource):
    """CustomerID"""

    @api.response(200, 'Ok', customer_model)
    @api.response(404, 'Customer not found')
    def get(self, customer_id):
        """Response to HTTP GET test"""
        if not check_api_key(flask.request.headers):
            REQUESTS_FAILED.inc()
            flask_restx.abort(401, "wrong API key")

        with conn.cursor() as cursor:
            try:
                cursor.execute("SELECT * FROM customers_schema.customers WHERE id = %s", (customer_id,))
                customer = cursor.fetchone()

                REQUESTS_SUCCESSFUL.labels(customer[0]).inc()
                return {
                    "id": customer[0],
                    "name": customer[1],
                    "phone": customer[2],
                    "email": customer[3],
                    "address": customer[4],
                    "city": customer[5],
                    "postalzip": customer[6],
                    "password": customer[7]
                }, 200
            except Exception:
                conn.rollback()
                REQUESTS_FAILED.inc()
                flask_restx.abort(404, "No customer with {} id found".format(customer_id))


@api.param('email_id', 'L\'email d\'un utilisateur')
@api.route('/customer/email/<string:email_id>')
class EmailID(flask_restx.Resource):
    """EmailID"""

    @api.response(200, 'Ok', customer_model)
    @api.response(404, 'Customer not found')
    def get(self, email_id):
        """Response to HTTP GET"""
        if not check_api_key(flask.request.headers):
            REQUESTS_FAILED.inc()
            flask_restx.abort(401, "wrong API key")

        with conn.cursor() as cursor:
            try:
                cursor.execute("SELECT * FROM customers_schema.customers WHERE email = %s", (email_id,))
                customer = cursor.fetchone()

                REQUESTS_SUCCESSFUL.labels(customer[0]).inc()
                return {
                    "id": customer[0],
                    "name": customer[1],
                    "phone": customer[2],
                    "email": customer[3],
                    "address": customer[4],
                    "city": customer[5],
                    "postalzip": customer[6],
                    "password": customer[7]
                }, 200
            except Exception:
                conn.rollback()
                REQUESTS_FAILED.inc()
                flask_restx.abort(404, "No customer with {} email found".format(email_id))


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
