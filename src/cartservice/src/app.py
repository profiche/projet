import flask
import flask_restx
from flask_restx import fields
import redis
import json
import os
from prometheus_client import make_wsgi_app, Counter
from werkzeug.middleware.dispatcher import DispatcherMiddleware

app = flask.Flask(__name__)
app.config['ERROR_404_HELP'] = False
app.config["SECRET_KEY"] = "69380d2425c27351894bde6886ec7c7f"
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {"/metrics": make_wsgi_app()})
api = flask_restx.Api(app=app,
                      doc="/doc",
                      title="API Gestion Panier", version="0.1",
                      description="API simple de gestion du panier des utilisateurs",
                      contact_email="Alan.Salmon@etu.univ-grenoble-alpes.fr")

API_KEY = os.environ["API_KEY"]
DB_HOST = os.environ["DB_HOST"]
DB_PORT = os.environ["DB_PORT"]
DB_NUM = os.environ["DB_NUM"]

r = redis.Redis(host=DB_HOST, port=DB_PORT, decode_responses=True, db=DB_NUM)

TOTAL_REQUESTS_BY_CLIENT_AND_METHOD = Counter("requests_by_client_and_method", "Number of requests by client and by method", labelnames=["customer_id", "method"])

item_model = api.model('Item', {
    'product_id': fields.String,
    'quantity': fields.Integer
})

cart_model = api.model('Cart', {
    'items': fields.List(fields.Nested(item_model)),
    'total_items': fields.Integer
})


def check_api_key(headers):
    H_API_KEY = headers.get("X-API-KEY")
    if not H_API_KEY:
        return False
    return H_API_KEY == API_KEY


@api.param('customer_id', 'L\'ID d\'un utilisateur')
@api.route('/cart/<string:customer_id>')
class CartCustomerID(flask_restx.Resource):
    """CartCustomerID"""

    @api.response(200, 'Ok', cart_model)
    @api.response(404, 'Cart not found')
    def get(self, customer_id):
        if not check_api_key(flask.request.headers):
            flask_restx.abort(401, "wrong API key")

        cart = r.get(customer_id)
        if not cart:
            flask_restx.abort(404, "Cart with customer ID {} not found".format(customer_id))
        TOTAL_REQUESTS_BY_CLIENT_AND_METHOD.labels(customer_id, "GET").inc()
        return json.loads(cart), 200

    @api.response(201, 'Added')
    @api.response(404, 'Cart not found')
    @api.response(406, 'Incorrect format')
    def post(self, customer_id):
        h = flask.request.headers['Content-Type']
        if h == 'application/json':
            json_data = flask.request.get_json()
            product_id = json_data.get("product_id")
            quantity = json_data.get("quantity")
            if not product_id or not isinstance(product_id, int):
                flask_restx.abort(406, "Missing Integer 'product_id' parameter.")
            if not quantity or not isinstance(quantity, int):
                flask_restx.abort(406, "Missing Integer 'quantity' parameter.")
            cart_json = r.get(customer_id)
            cart = {
                "items": [],
                "total_items": 0
            }
            if cart_json:
                cart = json.loads(cart_json)

            cart["total_items"] += quantity

            found = False
            for item in cart.get("items"):
                if item.get("product_id") == product_id:
                    found = True
                    item["quantity"] += quantity
            if not found:
                cart["items"].append({
                    "product_id": product_id,
                    "quantity": quantity
                })

            cart_json = json.dumps(cart)
            r.set(customer_id, cart_json)

            TOTAL_REQUESTS_BY_CLIENT_AND_METHOD.labels(customer_id, "POST").inc()

            return "Added", 201

        else:
            flask_restx.abort(406, "Incorrect format")

    @api.response(200, 'Ok')
    @api.response(404, 'Cart not found')
    def delete(self, customer_id):
        cart = r.get(customer_id)
        if not cart:
            flask_restx.abort(404, "Cart with customer ID {} not found".format(customer_id))
        r.delete(customer_id)
        TOTAL_REQUESTS_BY_CLIENT_AND_METHOD.labels(customer_id, "DELETE").inc()
        return "Deleted", 200


if __name__ == '__main__':
    app.run(debug=False, port=5000)
