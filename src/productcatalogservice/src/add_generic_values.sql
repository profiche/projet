-- Ajout des catégories génériques

INSERT INTO products_schema.categories (name, description) VALUES ('Canapés|Salon', 'Ensemble des canapés destinés au séjour');
INSERT INTO products_schema.categories (name, description) VALUES ('Tables à Manger|Salle à Manger', 'Ensemble de tables destinées au repas');
INSERT INTO products_schema.categories (name, description) VALUES ('Fauteuils|Salon', 'Ensemble de fauteuils destinés au séjour');
INSERT INTO products_schema.categories (name, description) VALUES ('Fauteuils|Bureau', 'Ensemble de fauteuils destinés au travail');
INSERT INTO products_schema.categories (name, description) VALUES ('Bureaux', 'Ensemble de bureaux destinés au travail');
INSERT INTO products_schema.categories (name, description) VALUES ('Lits', 'Au dodo !');
INSERT INTO products_schema.categories (name, description) VALUES ('Etagères|Déco', 'Ensemble d étagères décoratives');
INSERT INTO products_schema.categories (name, description) VALUES ('Luminaires', 'Et la lumière fût !');
INSERT INTO products_schema.categories (name, description) VALUES ('Chaises|Salle à manger', 'Ensemble de chaises destinés au repas');
INSERT INTO products_schema.categories (name, description) VALUES ('Chaises|Salon', 'Ensemble de chaises destinés au séjour');
INSERT INTO products_schema.categories (name, description) VALUES ('Miroirs|Déco', 'Ensemble de miroirs décoratifs');

-- Ajout des produits

INSERT INTO products_schema.products (pid, name, description, price) VALUES ('0PUK6V6EV0', 'Canapé Moderne en Cuir', 'Canapé élégant en cuir véritable avec des coutures raffinées', 899);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('1YMWWN1N4O', 'Canapé d''Angle Confortable', 'Canapé d''angle spacieux avec des coussins moelleux pour un confort ultime', 1099);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('2ZYFJ3GM2N', 'Table à Manger en Chêne Massif', 'Table à manger en chêne massif, parfaite pour les repas en famille', 499);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('9SIQT8TOJO', 'Fauteuil de Lecture Confortable', 'Fauteuil rembourré avec un design ergonomique pour une lecture détendue', 299);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('L9ECAV7KIM', 'Fauteuil Inclinable en Cuir', 'Fauteuil inclinable en cuir de qualité supérieure avec repose-pieds rétractable', 499);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('LS4PSXUNUM', 'Bureau Moderne en Verre et Acier', 'Bureau élégant en verre trempé et acier inoxydable pour un espace de travail contemporain', 449);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('OLJCESPC7Z', 'Bureau d''Angle avec Étagères', 'Bureau d''angle spacieux avec étagères pour une organisation optimale de votre espace de travail', 599);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('U6W2EWA94T', 'Lit King-Size avec Tête de Lit Rembourrée', 'Lit spacieux avec une tête de lit rembourrée pour une chambre à coucher luxueuse', 799);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('ZX4MQZH3C3', 'Lit Double en Bois Massif', 'Lit double en bois massif avec un design intemporel', 599);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('DEBJEBOUGY', 'Étagère Murale Minimaliste', 'Étagère murale flottante au design minimaliste pour afficher vos livres et objets de décoration', 59);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('Y7GU0AVYLG', 'Lampe de Table en Bois Naturel', 'Lampe de table artisanale en bois naturel pour éclairer votre espace avec élégance', 79);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('CVIG43KVJH', 'Suspension en Verre Soufflé', 'Suspension en verre soufflé avec un design artistique pour une ambiance lumineuse unique', 129);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('3CHWLUOIZI', 'Chaise de Salle à Manger Scandinave', 'Chaise au style scandinave avec pieds en bois massif et siège rembourré', 69);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('E57UMV3RPT', 'Chaise Pivotante en Cuir', 'Chaise pivotante en cuir élégant pour un confort et un style exceptionnels', 149);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('G61K9LADFI', 'Miroir Mural Design en Métal', 'Miroir mural au design contemporain avec un cadre en métal noir', 129);
INSERT INTO products_schema.products (pid, name, description, price) VALUES ('I3PAMAMZ1W', 'Miroir Rond en Rotin', 'Miroir rond en rotin pour une touche de nature dans votre décoration intérieure', 79);

-- Ajout du lien entre l'id des produits et des catégories

INSERT INTO products_schema.product_category (cid, pid) VALUES (1, '0PUK6V6EV0');
INSERT INTO products_schema.product_category (cid, pid) VALUES (1, '1YMWWN1N4O');
INSERT INTO products_schema.product_category (cid, pid) VALUES (2, '2ZYFJ3GM2N');
INSERT INTO products_schema.product_category (cid, pid) VALUES (3, '9SIQT8TOJO');
INSERT INTO products_schema.product_category (cid, pid) VALUES (4, 'L9ECAV7KIM');
INSERT INTO products_schema.product_category (cid, pid) VALUES (5, 'LS4PSXUNUM');
INSERT INTO products_schema.product_category (cid, pid) VALUES (5, 'OLJCESPC7Z');
INSERT INTO products_schema.product_category (cid, pid) VALUES (6, 'U6W2EWA94T');
INSERT INTO products_schema.product_category (cid, pid) VALUES (6, 'ZX4MQZH3C3');
INSERT INTO products_schema.product_category (cid, pid) VALUES (7, 'DEBJEBOUGY');
INSERT INTO products_schema.product_category (cid, pid) VALUES (8, 'Y7GU0AVYLG');
INSERT INTO products_schema.product_category (cid, pid) VALUES (8, 'CVIG43KVJH');
INSERT INTO products_schema.product_category (cid, pid) VALUES (9, '3CHWLUOIZI');
INSERT INTO products_schema.product_category (cid, pid) VALUES (10, 'E57UMV3RPT');
INSERT INTO products_schema.product_category (cid, pid) VALUES (11, 'G61K9LADFI');
INSERT INTO products_schema.product_category (cid, pid) VALUES (11, 'I3PAMAMZ1W');
