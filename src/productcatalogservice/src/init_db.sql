-- Créer le schéma "products_schema"
CREATE SCHEMA IF NOT EXISTS products_schema;

-- Créer la table "categories" dans le schéma "products_schema"
CREATE TABLE products_schema.categories (
    cid serial PRIMARY KEY,
    name text,
    description text
);

-- Créer la table "products" dans le schéma "products_schema"
CREATE TABLE products_schema.products (
    pid varchar(10) PRIMARY KEY,
    name text,
    description text,
    picture text,
    price double precision
);

-- Créer la table "product_category" dans le schéma "products_schema"
CREATE TABLE products_schema.product_category (
    pid varchar(10),
    cid integer,
    PRIMARY KEY (pid, cid),
    FOREIGN KEY (pid) REFERENCES products_schema.products(pid),
    FOREIGN KEY (cid) REFERENCES products_schema.categories(cid)
);