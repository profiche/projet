import flask
import flask_restx
import psycopg2
from flask_restx import fields
from prometheus_client import make_wsgi_app, Counter
from werkzeug.middleware.dispatcher import DispatcherMiddleware
import os

app = flask.Flask(__name__)
app.config['ERROR_404_HELP'] = False
app.config["SECRET_KEY"] = "69380d2425c27351894bde6886ec7c7f"
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {"/metrics": make_wsgi_app()})
api = flask_restx.Api(app=app,
                      title="API Gestion de produits", version="0.1",
                      description="API simple de gestion des produits",
                      contact_email="Alan.Salmon@etu.univ-grenoble-alpes.fr")

API_KEY = os.getenv("API_KEY", "toto")
POSTGRES_HOST = os.getenv("POSTGRES_HOST", "localhost")
POSTGRES_PORT = os.getenv("POSTGRES_PORT", "5432")
POSTGRES_DB = os.getenv("POSTGRES_DB", "products_db")
POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")

db_params = {
    'dbname': POSTGRES_DB,
    'user': POSTGRES_USER,
    'password': POSTGRES_PASSWORD,
    'host': POSTGRES_HOST,
    'port': int(POSTGRES_PORT)
}

categories_model = api.model('Categories', {
    'categories': flask_restx.fields.String
})

product_model = api.model('Product', {
    'id': fields.String,
    'name': fields.String,
    'description': fields.String,
    'price': fields.Integer
})

products_model = api.model('Products', {
    'products': fields.List(fields.Nested(product_model))
})

TOTAL_REQUESTS = Counter("total_requests", "Total requests made to products")
TOTAL_REQUESTS_BY_CATEGORIES = Counter("total_requests_by_categories", "Total requests by categories", labelnames=["category_name"])
TOTAL_REQUESTS_BY_PRODUCTS = Counter("total_requests_by_products", "Total requests by products", labelnames=["product_name"])
TOTAL_FAILED_REQUESTS_BY_PRODUCTS = Counter("total_failed_requests_by_products", "Total failed requests by products")

# Connexion à la base de données
conn = psycopg2.connect(**db_params)


def check_api_key(headers):
    H_API_KEY = headers.get("X-API-KEY")
    if not H_API_KEY:
        return False
    return H_API_KEY == API_KEY


@api.route('/categories')
class Categories(flask_restx.Resource):
    """Ressource Categories"""

    @api.response(200, 'Ok', categories_model)
    def get(self):
        """Response to HTTP GET"""
        if not check_api_key(flask.request.headers):
            flask_restx.abort(401, "wrong API key")

        cursor = conn.cursor()
        list_categories = []
        cursor.execute("SELECT name FROM categories")
        categories = cursor.fetchall()
        TOTAL_REQUESTS.inc()
        for category in categories:
            TOTAL_REQUESTS_BY_CATEGORIES.labels(category[0]).inc()
            list_categories.append(category[0])

        return {
            "categories": list_categories
        }, 200


@api.route('/products')
class Products(flask_restx.Resource):
    """Ressource Products"""

    @api.response(200, 'Ok', products_model)
    def get(self):
        """Response to HTTP GET"""
        if not check_api_key(flask.request.headers):
            flask_restx.abort(401, "wrong API key")

        cursor = conn.cursor()
        list_products = []
        cursor.execute("""SELECT products.pid, products.name, products.description, products.price,categories.name
                          FROM products inner join product_category on products.pid = product_category.pid inner join categories on product_category.cid = categories.cid""")
        products = cursor.fetchall()
        TOTAL_REQUESTS.inc()

        for product in products:
            categories = product[4].split("|")

            list_products.append({
                "id": product[0],
                "name": product[1],
                "description": product[2],
                "price": int(product[3]),
                "categories": categories
            })
            TOTAL_REQUESTS_BY_PRODUCTS.labels(product[1]).inc()
            for categorie in categories:
                TOTAL_REQUESTS_BY_CATEGORIES.labels(categorie).inc()

        print(list_products)
        return list_products, 200


@api.param('product_id', 'L\'ID d\'un produit')
@api.route('/product/<string:product_id>')
class ProductsID(flask_restx.Resource):
    """Ressource Products"""

    @api.response(200, 'Ok', product_model)
    @api.response(404, 'Product not found')
    def get(self, product_id):
        """Response to HTTP GET"""
        if not check_api_key(flask.request.headers):
            flask_restx.abort(401, "wrong API key")

        with conn.cursor() as cursor:
            try:
                cursor.execute("SELECT * FROM products WHERE pid = %s", (product_id,))
                product = cursor.fetchone()
                TOTAL_REQUESTS.inc()
                TOTAL_REQUESTS_BY_PRODUCTS.labels(product[1]).inc()

                return {
                    "id": product[0],
                    "name": product[1],
                    "description": product[2],
                    "price": int(product[3])
                }, 200
            except Exception:
                TOTAL_FAILED_REQUESTS_BY_PRODUCTS.inc()
                flask_restx.abort(404, "No product with {} id found".format(product_id))


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
