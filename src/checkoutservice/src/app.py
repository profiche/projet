from flask import Flask, request
from flask_restx import Api, Resource, fields
import flask_restx
import requests
import os
import pynats
from prometheus_client import make_wsgi_app, Counter
from werkzeug.middleware.dispatcher import DispatcherMiddleware

app = Flask(__name__)
app.config['ERROR_404_HELP'] = False
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {"/metrics": make_wsgi_app()})
api = flask_restx.Api(app=app,
                      doc="/doc",
                      title="API CheckoutService", version="1.50",
                      description="API CheckoutService",
                      contact_email="Ethan.profichet@etu.univ-grenoble-alpes.fr")

NATS_HOST = os.environ["NATS_HOST"]
NATS_PORT = os.environ["NATS_PORT"]
#Clé des APIs
API_KEY = os.environ["API_KEY"]
CARTSERVICE_API_KEY = os.environ["CARTSERVICE_API_KEY"]
CUSTOMERSSERVICE_API_KEY = os.environ["CUSTOMERSSERVICE_API_KEY"]
PRODUCTCATALOGSERVICE_API_KEY = os.environ["PRODUCTCATALOGSERVICE_API_KEY"]
PAYMENTSERVICE_API_KEY = os.environ["PAYMENTSERVICE_API_KEY"]
# Exemple de configuration des URL des services à modifier
CART_SERVICE_URL = os.environ["CARTSERVICE_URL"]
CUSTOMER_SERVICE_URL = os.environ["CUSTOMERSSERVICE_URL"]
PRODUCT_CATALOG_SERVICE_URL = os.environ["PRODUCTCATALOGSERVICE_URL"]
PAYMENT_SERVICE_URL = os.environ["PAYMENTSERVICE_URL"]
EMAIL_ORDER_SERVICE_URL = os.environ["EMAILORDERSERVICE_URL"]
SHIPPING_SERVICE_URL = os.environ["SHIPPINGSERVICE_URL"]

TOTAL_REQUESTS_BY_CLIENT = Counter("total_requests_by_client", "Total requests by client", labelnames=["customer_id"])


# Route order pour valider un panier
@api.route('/order')
class checkout(flask_restx.Resource):

    @api.response(201, 'Created')
    @api.response(401, 'Incorrect or missing API key')
    @api.response(404, 'Cart not found')
    def post(self):
        # Récupération des données de la commande
        order_data = request.get_json()

        # Récupération du panier du service CartService
        customer_id = order_data['customer_id']
        cart_url = f"{CART_SERVICE_URL}/cart/{customer_id}"
        response_cart = requests.get(cart_url)

        if response_cart.status_code == 404:
            flask_restx.abort(404, "Cart not found")

        cart_data = response_cart.json()

        # Récupération des informations du client auprès du service CustomerService
        customer_url = f"{CUSTOMER_SERVICE_URL}/customer/{customer_id}"
        response_customer = requests.get(customer_url)
        customer_data = response_customer.json()

        # Calcul du prix total de la commande en parcourant les éléments du panier
        order_total = 0
        items_total = 0

        for item in cart_data['items']:
            product_id = item['product_id']
            # Interroger le ProductCatalogService pour obtenir le prix du produit
            product_url = f"{PRODUCT_CATALOG_SERVICE_URL}/product/{product_id}"
            response_product = requests.get(product_url)
            product_data = response_product.json()
            product_price = product_data['price']

            #calculer le prix total en ajoutant le prix du produit
            order_total += product_price * item['quantity']
            items_total += item['quantity']

        # Demande de paiement au service PaymentService
        payment_url = f"{PAYMENT_SERVICE_URL}/process_payment"
        payment_data = {
            'amount': order_total,
            'credit_cart': order_data.get('credit_cart')
        }
        response_payment = requests.post(payment_url, json=payment_data)

        # Traitement du résultat du paiement
        if response_payment.status_code == 200:
            message = f'INFO:shippingservice:Sending package with {items_total} items to {customer_data["name"]} at {customer_data["address"]}, {customer_data["postalzip"]} {customer_data["city"]}'
            with pynats.NATSClient(f"nats://{NATS_HOST}:{NATS_PORT}") as client:
                client.connect()
                client.publish(subject="order", payload=bytes(message, encoding="utf-8"))
            # Suppression du panier du service CartService
            requests.delete(cart_url)
            TOTAL_REQUESTS_BY_CLIENT.labels(customer_id).inc()
            return 201, "Commande validée avec succès"
        else:
            # Le paiement a échoué
            flask_restx.abort(400, "Payment failed")

if __name__ == '__main__':
    app.run(debug=True, port=5000)