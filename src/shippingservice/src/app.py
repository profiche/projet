import pynats
import os

NATS_HOST = os.environ["NATS_HOST"]
NATS_PORT = os.environ["NATS_PORT"]


def callback(msg):
    print(msg.payload)


def wait_chat_message():
    with pynats.NATSClient(f"nats://{NATS_HOST}:{NATS_PORT}") as client:
        client.connect()

        client.subscribe(subject="order",
                         callback=callback)

        client.wait()


def main():
    wait_chat_message()


if __name__ == "__main__":
    main()
